import path from 'path';
import process from 'process'
import { defineConfig, type Plugin } from 'vite';
import { svelte, vitePreprocess } from '@sveltejs/vite-plugin-svelte';
import electron from 'vite-plugin-electron/simple';

export default defineConfig({
  server: {
    port: 5918,
    strictPort: true,
  },
  plugins: [
    svelte({
      preprocess: vitePreprocess(),
    }),
    electron({
      main: {
        entry: 'src/index.ts',
      },
      preload: {
        input: 'src/preload.ts',
      }
    }),
  ],
  resolve: {
    alias: {
      '$': path.resolve('src/renderer/src/'),
    },
  },
  root: path.resolve(process.cwd(), 'src/renderer'),
  base: './',
})
