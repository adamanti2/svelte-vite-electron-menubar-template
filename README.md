# Svelte + Vite + Electron + TypeScript + Menubar template

My template to quickly make personal menubar apps

Based on `https://github.com/kennethklee/clean-svelte-vite-electron`
and `https://github.com/max-mapper/menubar`

This repository is in the public domain
