// This file is the entry point for the Electron application.

import { app, BrowserWindow, nativeTheme } from 'electron';
import { menubar, Menubar } from 'menubar';

import { dirname } from 'node:path';
import { fileURLToPath } from 'node:url';
const __dirname = dirname(fileURLToPath(import.meta.url));

const DEVTOOLS = true;
const ICONS = {
    dark: `src/renderer/assets/icon/icon-white.png`,
    light: `src/renderer/assets/icon/icon-black.png`,
}

const WINDOW_OPTIONS = {
    width: 960,
    height: 540,
    // alwaysOnTop: true,
    // transparent: true,
    webPreferences: {
        preload: `${__dirname}/../dist-electron/preload.mjs`,
    }
}

let currentMenubar: Menubar | null = null;
function createMenubar() {
    let index = "";
    if (process.env.NODE_ENV !== 'development') {
        // Load production build
        index = `file://${__dirname}/renderer/dist/index.html`;
    } else {
        // Load vite dev server page 
        index = `http://localhost:5918/`;
    }
    let mb = menubar({
        index: index,
        browserWindow: WINDOW_OPTIONS,
        icon: nativeTheme.shouldUseDarkColors ? ICONS.dark : ICONS.light,
    });
    mb.on('ready', () => {
        mb.showWindow();
        mb.window?.setResizable(false);
        if (DEVTOOLS) mb.window?.webContents.openDevTools();
    });
    currentMenubar = mb;
    return mb;
}

app.whenReady()
    .then(() => {
        createMenubar()

        app.on('activate', function () {
            if (BrowserWindow.getAllWindows().length === 0) createMenubar()
        })
    })

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit()
});

nativeTheme.addListener('updated', function() {
    if (currentMenubar !== null) {
        currentMenubar.tray.setImage(nativeTheme.shouldUseDarkColors ? ICONS.dark : ICONS.light);
    }
});
