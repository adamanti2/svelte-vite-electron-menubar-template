import { contextBridge, ipcRenderer, type IpcRenderer } from 'electron';

declare global {
    interface Window {
      ipcRenderer: IpcRenderer
    }
}

contextBridge.exposeInMainWorld('ipcRenderer', ipcRenderer);
